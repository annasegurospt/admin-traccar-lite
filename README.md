# Admin Traccar Lite
> Um pequeno painel administrativo para plataformas de rastreamento Traccar.

[![Build Status][travis-image]][travis-url]
[![Downloads Stats][traccar-version]][npm-url]

O Admin Traccar Lite é um Dashboard minimalista com estatisticas de comunicação, resumo das informações dos veículos, cadastro mais eficiente e maior velocidade na busca das informações.

Suporta Traccar acima da versão 4.2.

NOTA: O projeto é novo e está em constante atualização. Se houver algum bug retorne periodicamente a esta página para verificar atualizações.


![](/uploads/f8a354259c6c15c53199798156a82eef/Captura_de_Tela_2019-12-16_às_09.18.29.png)

## Instalação de Dependências

-> Instale o Traccar seguindo as orientações no site  [https://traccar.org](https://traccar.org)
-> Após instalado acesse a pasta /opt/traccar/web

Debian/Ubuntu:

```sh
$ cd /opt/traccar/web
$ sudo apt install git -y
```

CentOS:

```sh
$ cd /opt/traccar/web
$ sudo yum install git
```

Windows:

```sh
- Acesse o https://git-scm.com/downloads para baixar o instalador do git. Escolha o sistema operacional que você pretende instalar (neste caso é o Windows) e baixe o instalador.
- Após o instalador ser baixado, execute-o.
- Clique em Next até página de seleção de componentes.
-> Escolha todos os componentes e clique em Next até o fim.
- Para finalizar, clique em Finish.
```

## Instalação da Ferramenta
Linux: 
```sh
$ git clone https://gitlab.com/michaelloliveira/admin-traccar-lite.git
$ mv admin-traccar-lite admin
$ chown www-data:www-data admin
$ sudo systemctl restart traccar
```

Windows: 
```sh
- Execute o programa Prompth do Windows em modo Administrador
$ cd C:\Program Files\Traccar\Web
$ git clone https://gitlab.com/michaelloliveira/admin-traccar-lite.git
$ ren admin-traccar-lite admin

Ou

- Através do Windows Explorer acesse a pasta "web" no diretório de instalação do Traccar
- Realize download da ferramenta
- Descompacte o arquivo dentro da pasta "web"
- Renomeie a pasta "admin-traccar-lite" para "admin"
```

-> Acesse

http://seuendereco/admin

_Para mais detalhes, dicas e truque, consulte a [Wiki][wiki]._ 

## Atualização


```sh
$ cd /opt/traccar/web/admin/
$ git pull
```

NOTA: Limpe o cache do seu navegador sempre que houver uma atualização

## Histórico de lançamentos

* 1.0.1
    * CONSERTADO: Notificações web em tempo real.
    * ADD: Substituição das funções `loadDevices()`, `loadUsers()`, `loadGroups()` por uma global `load()`
* 1.0.0
    * O primeiro lançamento adequado
* 0.0.1
    * Primeira Versão

## Meta

Michaell Oliveira – [@Solutions2uTec](https://twitter.com/Solutions2uTec) – michaelloliveira@gmail.com.com
[https://gitlab.com/michaelloliveira/admin-traccar-lite](https://gitlab.com/michaelloliveira/admin-traccar-lite/)

Tenha sua plataforma de rastreamento com suporte especializado.
[Saiba mais em: Central de Rastreamento GPS](https://www.centralderastreamentogps.com.br)

## AJUDE O PROJETO CRESCER, FAÇA UMA DOAÇÃO PARA INCENTIVAR O DESENVOLVIMENTO DE NOVOS RECURSOS E VERSÕES.

[CLIQUE PARA DOAR](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=C4D8MWM2U9ZAG&source=url) 


[npm-url]: https://npmjs.org/package/datadog-metrics
[traccar-version]: https://img.shields.io/badge/Traccar-v4.6-blue
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/michaelloliveira/admin-traccar-lite/wiki




